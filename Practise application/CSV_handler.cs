﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Google.Protobuf;
using Google.Protobuf.Reflection;

namespace Practise_application
{
    class CSV_handler : IFile
    {
        public People parse_file(string[] file_data)
        {
            //convert csv format into protobuf

            var test_people = parse_csv(file_data);

            //Make this output file format. this can be sent to listener
            //using (var output = File.Create("test.csv"))
            //{
            //    test_people.WriteTo(output);
            //}
            var data_to_send = test_people.ToString();
            var bytes_test = test_people.ToByteArray();

            return test_people;
        }

        private People parse_csv(string[] csv_lines)
        {
                        //Normally this would be achieved using Textfieldparser
            //string[] delimiters = { ";", ",", "-" };
            //using (TextFieldParser parser =
            //    FileSystem.OpenTextFieldParser("Names.txt", delimiters))
            //{
            //    // Process the file's lines.
            //    while (!parser.EndOfData)
            //    {
            //        try
            //        {
            //            string[] fields = parser.ReadFields();
            //            ListViewItem item = lvwPeople.Items.Add(fields[0]);
            //            for (int i = 1; i <= 5; i++)
            //            {
            //                item.SubItems.Add(fields[i]);
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            MessageBox.Show(ex.Message);
            //        }
            //    }
            //}

            People test_people = new People { };

            string[] headers = new string[] { };

            foreach (var line in csv_lines)
            {
                if (line.Length != 0)
                {

                    if (headers.Length == 0)
                    {
                        headers = line.Split(',');
                    }
                    else
                    {
                        var fields = line.Split(',');

                        Person test = new Person
                        {
                            Id = Int32.Parse(fields[0]),
                            FirstName = fields[1],
                            SecondName = fields[2],
                            Organisation = fields[3]
                        };

                        test_people.Person.Add(test);
                    }
                }
            }
            return test_people;
        }
    }
}
