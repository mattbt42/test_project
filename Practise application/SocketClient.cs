﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Sockets;
using System.Net;
using Google.Protobuf;

namespace Practise_application
{
    class SocketClient
    {
        public static void Example(IPAddress ipAddress, Byte[] data_to_send)
        {
            SendData(ipAddress, data_to_send);
        }


        public static void SendData(IPAddress ipAddress, Byte[] data_to_send)
        {
            //data_to_send = "This is a test<EOF>";
            byte[] bytes = new byte[1024];

            try
            {
                // Connect to a Remote server  

                IPEndPoint remoteEP = new IPEndPoint(ipAddress, 11111);

                Socket client = new Socket(ipAddress.AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                // Connect the socket to the remote endpoint. Catch any errors.    
                try
                {
                    // Connect to Remote EndPoint  
                    client.Connect(remoteEP);

                    Console.WriteLine("Connected to {0}", client.RemoteEndPoint.ToString());

                    // string to bytes and store in array   
                    int bytesSent = client.Send(data_to_send);
                    client.Send(Encoding.ASCII.GetBytes("<EOF>"));
                    int bytesRec = client.Receive(bytes);
                    int er = client.Receive(bytes);

                    // Receive the response from the remote device.    
                    Console.WriteLine("Response = {0}", Encoding.ASCII.GetString(bytes, 0, bytesRec));
                    client.Shutdown(SocketShutdown.Both);
                    client.Close();

                }
                catch (ArgumentNullException ane)
                {
                    Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
                }
                catch (SocketException se)
                {
                    Console.WriteLine("SocketException : {0}", se.ToString());
                }
                catch (Exception e)
                {
                    Console.WriteLine("Unexpected exception : {0}", e.ToString());
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
