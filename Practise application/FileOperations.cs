﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Practise_application
{
    public class FileOperations
    {

        private const uint GENERIC_READ = 0x80000000;
        private const int GENERIC_WRITE = 0x40000000;
        private const uint OPEN_EXISTING = 3;
        private const uint CREATE_NEW = 1;
        private const int FILE_ATTRIBUTE_NORMAL = 0x80;
        //const UInt32 FILE_FLAG_OVERLAPPED = 0x40000000;
        //const UInt32 FILE_FLAG_NO_BUFFERING = 0x20000000;


        public static bool WriteToFile(string file_path, string data_to_write)
        {
            uint LenWritten;
            var file_exists = false;
            // check for file

            SafeFileHandle file_handle;


            if(file_exists)
            {
                file_handle = CreateFileA(file_path, GENERIC_WRITE, 0, IntPtr.Zero, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, IntPtr.Zero);
            }
            else
            {
                file_handle = CreateFileA(file_path, GENERIC_WRITE, 0, IntPtr.Zero, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, IntPtr.Zero);
            }

            var flag = WriteFile(file_handle, data_to_write, data_to_write.Length, out LenWritten, IntPtr.Zero);

            return flag;
        }

        //public static bool ReadFromFile(string file_path)
        public static string[] ReadFromFile(string file_path)
        {

            uint LenRead;
            // long to int here?
            var file_len = new System.IO.FileInfo(file_path).Length;

            byte[] read_data = new byte[file_len];
            SafeFileHandle file_read_handle = CreateFileA(file_path, GENERIC_READ, 0, IntPtr.Zero, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, IntPtr.Zero);
            var file_read = ReadFile(file_read_handle, read_data, Convert.ToInt32(file_len), out LenRead, IntPtr.Zero);

            string file_string = Encoding.UTF8.GetString(read_data, 0, read_data.Length);

            string[] lines = file_string.Split(
                new[] { Environment.NewLine },
                StringSplitOptions.None
            );


            //return file_read;
            //return read_data;
            return lines;
        }


        //public static void example_test()
        //{


        //    SafeFileHandle file_handle = CreateFileA(".test_file.txt", GENERIC_WRITE, 0, IntPtr.Zero, CREATE_NEW, FILE_ATTRIBUTE_NORMAL, IntPtr.Zero);


        //    string DataBuffer = "test data";
        //    string DataBuffer2 = "testing 2";
        //    int dwBytesToWrite = 10;
        //    uint LenWritten;

        //    var flag = WriteFile(file_handle, DataBuffer, dwBytesToWrite, out LenWritten, IntPtr.Zero);

        //    var success_bool = CloseHandle(file_handle);

        //    SafeFileHandle file_handle2 = CreateFileA(".test_file.txt", GENERIC_WRITE, 0, IntPtr.Zero, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, IntPtr.Zero);

        //    var flag2 = WriteFile(file_handle2, DataBuffer2, dwBytesToWrite, out LenWritten, IntPtr.Zero);

        //    var success_bool2 = CloseHandle(file_handle2);

        //    SafeFileHandle file_read_handle = CreateFileA(".test_file.txt", GENERIC_READ, 0, IntPtr.Zero, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, IntPtr.Zero);

        //    //String[] read_data = new string[] { };



        //    //FILE_SEGMENT_ELEMENT fseg = new FILE_SEGMENT_ELEMENT();

        //    byte[] read_data = new byte[dwBytesToWrite];

        //    var file_read = ReadFile(file_read_handle, read_data, dwBytesToWrite, out LenWritten, IntPtr.Zero);


        //    Console.WriteLine(Encoding.Default.GetString(read_data, 0, read_data.Length));



        //    var pie = read_data;

        //}


        [DllImport("Kernel32.dll")]
        static extern SafeFileHandle CreateFileA(String lpFileName,
                                        uint dwDesiredAccess,
                                        uint dwShareMode,
                                        IntPtr lpSecurityAttributes,
                                        uint dwCreationDisposition,
                                        uint dwFlagsAndAttributes,
                                        IntPtr hTemplateFile);

        [DllImport("Kernel32.dll")]
        static extern bool WriteFile(SafeHandle hFile,
                                string lpBuffer,
                                int dwBytesToWrite,
                                out uint dwBytesWritten,
                                IntPtr hTemplateFile);

        [StructLayout(LayoutKind.Explicit, Size = 8)]
        internal struct FILE_SEGMENT_ELEMENT
        {
            [FieldOffset(0)]
            public IntPtr Buffer;
            [FieldOffset(0)]
            public UInt64 Alignment;
        }


        
        [DllImport("kernel32.dll", SetLastError = true)]
        static extern bool ReadFile(SafeHandle hFile, [Out] byte[] lpBuffer,
           int nNumberOfBytesToRead, out uint lpNumberOfBytesRead, IntPtr lpOverlapped);


        [DllImport("kernel32.dll", SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        static extern bool CloseHandle(SafeHandle hObject);

    }

}
